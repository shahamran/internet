/* The keys in the calculator */
var calcKeys = [
  7, 8, 9, "+",
  4, 5, 6, "-",
  1, 2, 3, "/",
  0, ".", "=", "*"
];

/* The available operators */
var operators = [
  "+", "-", "/", "*"
];

/* The calculator elements */
var calcElements = {
  "tagName": "section",
  "class": "instance",
  "content": [
    {
      "tagName": "header",
      "content": [
        {
          "tagName": "div",
          "class": "screen",
          "content": ["0"]
        },
        {
          "tagName": "span",
          "class": "clear",
          "content": ["C"]
        }
      ]
    },
    {
      "tagName": "section",
      "class": "keys"
    }
  ]
};

/**
 * The calculators constructor
 */
function Calc() {
  // Create HTML and get handles to the created objects
  let $calc = renderElement(calcElements, $("#calculator"));
  let $keys = $calc.children(".keys");
  let $header = $calc.children("header");
  let $screen = $header.children(".screen");
  // Make the C button clear the screen
  $header.children(".clear").click(function() {
    $screen.text("0");
  });

  // Define what "=" does
  var calculate = function(input) {
    if (operators.includes(input.slice(-1))) {
      input = input.slice(0, -1);
    }
    if (input === '') {
      input = '0';
    }
    /* I know this is dangerous, but it's a toy example. */
    $screen.text(eval(input));
  };

  // I rely on the existence of the $ handles, hence the var = function...
  var keyPress = function(event) {
    keyVal = $(this).text();
    input = $screen.text();
    if (keyVal === '=') {
      calculate(input);
    } else if (!isNaN(keyVal) && input === '0' ||
               keyVal === '-' && input === '0') {
      $screen.text(keyVal);
    } else {
      let lastChar = input.slice(-1);
      if (operators.includes(keyVal) &&
          operators.includes(lastChar)) {
        $screen.text(input.replace(lastChar, keyVal));
      } else if (keyVal !== "." || !input.includes(".")) {
        $screen.text(input + keyVal);
      }
    }
  };

  // Add buttons
  for (let i=0; i < calcKeys.length; ++i) {
    let key = calcKeys[i];
    let $key = $('<span/>');
    if (isNaN(key)) {
      if (key === '=') {
        $key.addClass('eval');
      } else if (key !== '.') {
        $key.addClass('operator');
      }
    }
    $key.text(key).click(keyPress).appendTo($keys);
  }
}
