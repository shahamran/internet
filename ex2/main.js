/**
 * The following section imports jquery
 */
function loadScript(url, callback) {
  var script = document.createElement('script');
  script.type = "text/javascript";
  script.onload = function() {
    callback();
  };
  script.src = url;
  document.getElementsByTagName('head')[0].appendChild(script);
}

loadScript('jquery.min.js',
          function() {
            loadScript('calculator.js', start);
          });

/**
 * My actual code starts here.
 */

/* This is appended to the page title */
var pageName = "Ex2";

/* A dictionary of valid logins (1 in this case) */
var VALID_USERS = {"admin": "admin"};

/* -------------------- pages JSON -------------------- */
var loginPage = {
  "tagName": "div",
  "id": "login",
  "content": [
    {
      "tagName": "input",
      "id": "username",
      "placeholder": "Enter Username"
    },
    {
      "tagName": "input",
      "id": "password",
      "type": "password",
      "placeholder": "Enter Password"
    },
    {
      "tagName": "button",
      "id": "login_btn",
      "content": ["Login"]
    }
  ]
};

var profilePage = {
  "tagName": "div",
  "id": "profile",
  "content": [
    {
      "tagName": "header",
      "content": [
        {
          "tagName": "h1",
          "content": ["Profile Page - Ran J. Shaham"]
        },
        {
          "tagName": "h2",
          "content": ["I.D. 203781000"]
        }
      ]
    },
    {
      "tagName": "section",
      "id": "main_content",
      "content": [
        {
          "tagName": "aside",
          "content": [
            {
              "tagName": "img",
              "width": "200",
              "height": "240",
              "src": "images/ranziskaner.jpeg",
              "title": "All rights reserved to Laure Scemama",
            }
          ]
        },
        {
          "tagName": "h1",
          "content": ["Fun Fact"]
        },
        {
          "tagName": "p",
          "content": [
            "Sometimes, when I'm extra bored, I add a ",
            {
              "tagName": "q",
              "content": ["J"]
            },
            " to my full name. It may or may not be related to",
            " the freshly elected president of the ",
            {
              "tagName": "abbr",
              "title": "United States",
              "content": ["U.S."]
            }
          ]
        },
        {
          "tagName": "p",
          "content": [
            "Also, if I were to brew my own ",
            {
              "tagName": "a",
              "href": "https://en.wikipedia.org/wiki/Wheat_beer#Weissbier",
              "content": [
                {
                  "tagName": "dfn",
                  "title": "Beer which is brewed with a large proportion of wheat",
                  "content": ["Weissbier"]
                }
              ]
            },
            ", I would name it ",
            {
              "tagName": "q",
              "content": ["Ranziskaner"]
            }
          ]
        }
      ]
    },
    {
      "tagName": "footer",
      "content": [
        {
          "tagName": "p",
          "content": [
            "My favorite page in the web is probably ",
            {
              "tagName": "a",
              "href": "http://moodle2.cs.huji.ac.il/nu16/",
              "content": ["moodle"]
            },
            " - since according to firefox,",
            " it's my top visited page."
          ]
        },
        {
          "tagName": "button",
          "id": "calc_btn",
          "content": ["Calculator!!!"]
        }
      ]
    }
  ]
};

var calcPage = {
  "tagName": "div",
  "id": "calculator",
  "content": [
    {
      "tagName": "button",
      "id": "add_calc_btn",
      "content": ["Add Calculator"]
    }
  ]
};
/* -------------------- end JSON -------------------- */


/**
 * Render objects with a "tagName" field as DOM elements and append
 * them to the given "parent" element.
 * Continue recursively for each object in the "content" field.
 */
function renderElement(elem, parent) {
  let $elem = $("<" + elem.tagName + "/>");
  $elem.appendTo(parent);
  $elem.text("");
  for (let key in elem) {
    if (!(elem.hasOwnProperty(key))) {
      continue;
    } else if (key == "tagName") {
      continue;
    } else if (key == "content") {
      let content = elem.content;
      for (let i = 0; i < content.length; ++i) {
        if ($.type(content[i]) === "string") {
          $elem.html($elem.html() + content[i]);
        } else {
          renderElement(content[i], $elem);
        }
      }
    } else {
      $elem.attr(key, elem[key]);
    }
  }
  return $elem;
}

/**
 * Hide the given (selector of an) element
 */
function hideScreen(toHide) {
  $(toHide).removeClass('visible').addClass('hidden');
}

/**
 * Show the given element
 */
function showScreen(toShow) {
  $(toShow).removeClass('hidden').addClass('visible');
}

/**
 * Switch between two elements.
 * Also change the page title to "title".
 */
function switchScreen(toHide, toShow, title) {
  $('head > title').text(title + " | " + pageName);
  hideScreen(toHide);
  showScreen(toShow);
}

/**
 * Return true iff the user and pass match the valid users list
 */
function isValidLogin(user, pass) {
  return VALID_USERS[user] === pass;
}

/**
 * Determine the behavior of the Login button
 */
function handleLoginClick() {
  var username = $('#username').val();
  var pass = $('#password').val();
  if (isValidLogin(username, pass)) {
    // If credentials are O.K., transfer to the profile page
    switchScreen('#login', '#profile', 'Profile');
  }
}

/**
 * Determine the behavior of the Calculator button
 */
function handleCalcClick() {
  // Just switch to the calculator page.
  switchScreen('#profile', '#calculator', 'Calculator');
}

/**
 * Start everything
 */
function start() {
  // Add DOM elements
  $('<meta/>')
    .attr('charset', 'utf-8')
    .appendTo(document.head);
  renderElement(loginPage, document.body);
  renderElement(profilePage, document.body);
  renderElement(calcPage, document.body);
  // Change visibility to the initial values
  hideScreen('#profile');
  hideScreen('#calculator');
  switchScreen('', '#login', 'Login');
  // Add behavior to buttons
  $('#login_btn').click(handleLoginClick);
  $('#calc_btn').click(handleCalcClick);
  $("#add_calc_btn").click(function() {
    new Calc();
  });
}
