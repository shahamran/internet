# README FOR INTERNET - EX1 - RAN SHAHAM

## Directory Structure:
- ex1/
    + main.html
    + readme.txt
    + styles
        * style.css
    + images
        * ranziskaner.jpeg

## Styles I used:
1. color
2. transition
3. margin
4. width
5. font-size
6. font-family
7. font-weight
8. text-decoration
9. text-align
10. float
- Also, I used a custom font using an at-rule (@import)

## Tags I used:
1. html
2. head
3. title
4. body
5. article
6. header
7. h1
8. h2
9. section
10. aside
11. img
12. p
13. abbr
14. a
15. dfn
16. q
17. footer
